#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """v...>>.vv>
.vv>>.vv..
>>.>v>...v
>>v>>.>.v.
v>v.vv.v..
>.>>..v...
.vv..>.>v.
v.v..>>v.v
....v..v.>"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 58)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 50)
		pass

def parse_input(i):
	out = []
	for line in i:
		new = []
		for c in line:
			if c == ">":
				new.append(1)
			elif c == "v":
				new.append(2)
			elif c == ".":
				new.append(0)
		out.append(new)
	return out

def move_once(old):
	new = list([o.copy() for o in old])
	moved = False
	for y in range(len(old)):
		for x in range(len(old[0])):
			if old[y][x] == 1:
				try:
					if old[y][x + 1] == 0:
						moved = True
						new[y][x] = 0
						new[y][x + 1] = 1
				except:
					if old[y][0] == 0:
						moved = True
						new[y][x] = 0
						new[y][0] = 1
	
	new_2 = list([o.copy() for o in new])
	for y in range(len(new)):
		for x in range(len(new[0])):
			if new[y][x] == 2:
				try:
					if new[y + 1][x] == 0:
						moved = True
						new_2[y][x] = 0
						new_2[y + 1][x] = 2
				except:
					if new[0][x] == 0:
						moved = True
						new_2[y][x] = 0
						new_2[0][x] = 2
	return new_2, moved

def part1(i):
	idx = 0
	new = i
	while True:
		new, moved = move_once(new)
		idx += 1
		if not moved:
			break
	return idx

def part2(i):
	print("Christmas is saved!")
	return 50

def main():
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	print(part2(i))

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
