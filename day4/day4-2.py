#!/usr/bin/env python3

import sys
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.b = Board([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24])
		self.marked = dict.fromkeys(range(0, 25), False)
		self.marked[19] = True
		self.marked[11] = True
		self.marked[15] = True
		self.marked[16] = True
		self.marked[17] = True
		self.marked[0] = True
		self.marked[24] = True
		self.marked[18] = True
	
	def test_board_creation(self):
		self.assertEqual(self.b.values, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24])
		self.assertEqual(self.b.goals[0:4], [[0, 1, 2, 3, 4], [0, 5, 10, 15, 20], [5, 6, 7, 8, 9], [1, 6, 11, 16, 21]])
	
	def test_apply_value(self):
		self.b.apply_value(5)
		self.assertEqual(self.b.goals[0:4], [[0, 1, 2, 3, 4], [0, 10, 15, 20], [6, 7, 8, 9], [1, 6, 11, 16, 21]])
	
	def test_board_won(self):
		self.b.apply_value(0)
		self.assertFalse(self.b.board_won())
		self.b.apply_value(1)
		self.assertFalse(self.b.board_won())
		self.b.apply_value(2)
		self.assertFalse(self.b.board_won())
		self.b.apply_value(3)
		self.assertFalse(self.b.board_won())
		self.b.apply_value(4)
		self.assertTrue(self.b.board_won())
	
	def test_get_score(self):
		self.assertEqual(self.b.get_score(self.marked, 18), 3240)
		self.marked[10] = True
		self.assertEqual(self.b.get_score(self.marked, 10), 1700)

class Board:
	def __init__(self, values):
		self.values = values
		self.goals = []
		for i in range(5):
			self.goals = extend_with_range(values, self.goals, 0 + (5 * i), 4 + (5 * i), 1)
			self.goals = extend_with_range(values, self.goals, (0 + i), (20 + i), 5)
	
	def apply_value(self, value):
		"""Removes the given value from all goals it exists in"""
		for idx, goal in enumerate(self.goals):
			self.goals[idx] = list(filter(lambda x: x != value, goal))
	
	def board_won(self):
		"""Checks if any goals on the board have been completed"""
		return any([len(x) == 0 for x in self.goals])
	
	def get_score(self, marked_values, most_recent):
		"""Given the list of marked values and the most recent value, determines
		   the board's total score"""
		return sum([x for x in self.values if not marked_values[x]]) * most_recent
			
def extend_with_range(values, arr, start, end, step):
	"""Extends arr with values from values in the range (start, stop, step), with stop inclusive"""
	out = []
	for i in range(start, end + step, step):
		out.append(values[i])
	arr.append(out)
	return arr

def main():
	if len(sys.argv) < 2:
		sys.exit(0)
	draws = []
	boards = []
	with open(sys.argv[1], "r") as f:
		draws = list([int(x) for x in f.readline().split(",")])
		new_board = []
		i = 0
		for line in f.readlines():
			if (i % 6) == 0 and len(new_board) > 0:
				boards.append(Board(new_board))
				new_board = []
			elif (i % 6) > 0:
				new_board.extend([int(x) for x in line.split()])
			i += 1
	marked_values = dict.fromkeys(draws, False)
	while len(boards) > 1:
		next_val = draws.pop(0)
		marked_values[next_val] = True
		idx = 0
		while idx < len(boards):
			b = boards[idx]
			b.apply_value(next_val)
			if b.board_won():
				del boards[idx]
				idx -= 1
			idx += 1
	while not boards[0].board_won():
		next_val = draws.pop(0)
		marked_values[next_val] = True
		boards[0].apply_value(next_val)
	print(boards[0].get_score(marked_values, next_val))

if __name__ == '__main__':
	#unittest.main()
	main()
