#!/usr/bin/env python3

import sys

def main():
	result = 0
	prev_val = 0
	cur_val = 0
	if len(sys.argv) < 2:
		sys.exit(0)
	with open(sys.argv[1], "r") as f:
		prev_val = int(f.readline())
		for line in f.readlines():
			cur_val = int(line)
			if cur_val > prev_val:
				result += 1
			prev_val = cur_val
	print(result)

if __name__ == '__main__':
	main()
