#!/usr/bin/env python3

import sys

def main():
	result = 0
	cur_val = 0
	prev_sum = 0
	window = []
	window_len = 3
	if len(sys.argv) < 2:
		sys.exit(0)
	with open(sys.argv[1], "r") as f:
		while len(window) < window_len:
			window.append(int(f.readline()))
		for line in f.readlines():
			cur_val = int(line)
			new_sum = prev_sum - window[0] + cur_val
			if new_sum > prev_sum:
				result += 1
			window.pop(0)
			window.append(cur_val)
			prev_sum = new_sum
	print(result)

if __name__ == '__main__':
	main()
