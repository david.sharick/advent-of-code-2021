#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		#self.assertEqual(part1(self.test), y)
		pass
		
	def test_part2(self):
		#self.assertEqual(part2(self.test), y)
		pass

def parse_input(i):
	for line in i:
		pass
	return

def part1(i):
	return 0

def part2(i):
	return 0

def main():
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	print(part2(i))

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
