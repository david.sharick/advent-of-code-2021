#!/usr/bin/env python3

import ast
from collections import defaultdict, Counter
from functools import lru_cache
import math
import string
import sys
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]
[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]
[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]
[[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]
[7,[5,[[3,8],[1,4]]]]
[[2,[2,2]],[8,[8,1]]]
[2,9]
[1,[[[9,3],9],[[9,0],[0,7]]]]
[[[5,[7,4]],7],1]
[[[[4,2],2],6],[8,7]]"""
		self.test = parse_input(self.testinput.split("\n"))
		self.testi2 = """[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
[[[5,[2,8]],4],[5,[[9,9],0]]]
[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
[[[[5,4],[7,7]],8],[[8,3],8]]
[[9,3],[[9,9],[6,[4,9]]]]
[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]"""
		self.test2 = parse_input(self.testi2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 3488)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 3993)
		pass

def parse_input(i):
	out = []
	for line in i:
		out.append(ast.literal_eval(line.strip()))
	return out

def length(num):
	"""Gives the number of pure numbers in the given snail number"""
	if isinstance(num, list):
		return sum([length(n) for n in num])
	else:
		return 1

def snailnum_ref(num, n):
	"""Gives the pure number that is at position n in the flattened snail number"""
	if isinstance(num, list):
		if n < length(num[0]):
			return snailnum_ref(num[0], n)
		else:
			return snailnum_ref(num[1], n - length(num[0]))
	else:
		if n > 0:
			print("Fail")
		return num

def snailnum_modify(num, n, new_val):
	"""Changes the pure number at position n in the flattened snail number to new_val"""
	if isinstance(num, list):
		if n < length(num[0]):
			return [snailnum_modify(num[0], n, new_val), num[1]]
		else:
			return [num[0], snailnum_modify(num[1], n - length(num[0]), new_val)]
	else:
		if n > 0:
			print("Fail")
		return new_val

def snailnum_replace(num, path, new_val):
	"""Replaces the snail number given by the path with new_val"""
	if len(path) > 0:
		if path[0] == 0:
			return [snailnum_replace(num[0], path[1:], new_val), num[1]]
		elif path[0] == 1:
			return [num[0], snailnum_replace(num[1], path[1:], new_val)]
	else:
		return new_val

def earliest_explode(num, acc):
	"""Gives the earliest exploding pair as both refs and path, or None if none exists"""
	if isinstance(num, int):
		return None
	first_ref = 0
	secnd_ref = length(num[0])
	if isinstance(num[0], int) and isinstance(num[1], int) and acc >= 4: # we are the exploding pair
		return [0, 1, []] # as far as we know, these are the indices and there is no path to us
	else:
		first_explode = earliest_explode(num[0], acc + 1)
		if first_explode is None:
			pass
		else:
			# we have an explosion there
			new_path = [0]
			new_path.extend(first_explode[2])
			return [first_explode[0], first_explode[1], new_path]
		secnd_explode = earliest_explode(num[1], acc + 1)
		if secnd_explode is None:
			pass
		else:
			# we have an explosion there
			new_path = [1]
			new_path.extend(secnd_explode[2])
			return [secnd_ref + secnd_explode[0], secnd_ref + secnd_explode[1], new_path]
	# please work

def splitting_path(num):
	"""Gives the path to a number that can split in the num, or None if none exists"""
	if isinstance(num, int):
		if num >= 10:
			return True
		else:
			return None
	else:
		left_path = splitting_path(num[0])
		if left_path is True:
			return [0]
		elif isinstance(left_path, list):
			out = [0]
			out.extend(left_path)
			return out
		right_path = splitting_path(num[1])
		if right_path is True:
			return [1]
		elif isinstance(right_path, list):
			out = [1]
			out.extend(right_path)
			return out
	return None

def split(num, path):
	"""Splits the number at the given path"""
	if len(path) > 0:
		nxt = path.pop(0)
		if nxt == 0:
			return [split(num[0], path), num[1]]
		elif nxt == 1:
			return [num[0], split(num[1], path)]
	else:
		to_split = num
		return [to_split // 2, math.ceil(to_split / 2)]

def reduce(num):
	"""Fully reduces a snail number"""
	mod = False
	while True:
		exploding_pair = earliest_explode(num, 0) # contains ref to two exploding values, plus path to pair itself, should be 1 apart
		if exploding_pair is not None:
			if exploding_pair[0] > 0:
				num = snailnum_modify(num, exploding_pair[0] - 1, snailnum_ref(num, exploding_pair[0] - 1) + snailnum_ref(num, exploding_pair[0]))
			if exploding_pair[1] < length(num) - 1:
				num = snailnum_modify(num, exploding_pair[1] + 1, snailnum_ref(num, exploding_pair[1] + 1) + snailnum_ref(num, exploding_pair[1]))
			num = snailnum_replace(num, exploding_pair[2], 0)
			mod = True
			continue
		split_path = splitting_path(num)
		if split_path is not None:
			num = split(num, split_path)
			continue
		break
	return num

def magnitude(num):
	"""Gives the magnitude of a snail number"""
	if isinstance(num, int):
		return num
	else:
		return (3 * magnitude(num[0])) + (2 * magnitude(num[1]))

def add_snailnums(fst, snd):
	"""Adds the two snail numbers and reduces their sum"""
	unreduced = [fst, snd]
	reduced = reduce(unreduced)
	return reduced

def part1(i):
	final_num = i.pop(0)
	while len(i) > 0:
		nxt = i.pop(0)
		final_num = add_snailnums(final_num, nxt)
	return magnitude(final_num)

def part2(i):
	curr_best = 0
	for x in range(len(i)):
		#print(x)
		for y in range(len(i)):
			if x != y:
				curr_best = max(magnitude(add_snailnums(i[x], i[y])), magnitude(add_snailnums(i[y], i[x])), curr_best)
	return curr_best

def main():
	if len(sys.argv) < 2:
		sys.exit(0)
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))

if __name__ == '__main__':
	#unittest.main()
	main()
