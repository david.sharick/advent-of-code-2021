#!/usr/bin/env python3

import sys

def modify_pos(hpos, vpos, aim, line):
	"""Applies the command in the given line to the position"""
	tokens = line.split(" ")
	cmd = tokens[0]
	val = tokens[1]
	if cmd == "forward":
		hpos += int(val)
		vpos += aim * int(val)
	elif cmd == "down":
		aim += int(val)
	elif cmd == "up":
		aim -= int(val)
	else:
		print(f"Invalid tokens {tokens}")
		sys.exit(0)
	return hpos, vpos, aim

def main():
	hpos = 0
	vpos = 0
	aim = 0
	if len(sys.argv) < 2:
		sys.exit(0)
	with open(sys.argv[1], "r") as f:
		for line in f.readlines():
			hpos, vpos, aim = modify_pos(hpos, vpos, aim, line)
	print(hpos * vpos)

if __name__ == '__main__':
	main()
