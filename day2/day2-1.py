#!/usr/bin/env python3

import sys

def modify_pos(hpos, vpos, line):
	"""Applies the command in the given line to the position"""
	tokens = line.split(" ")
	if tokens[0] == "forward":
		hpos += int(tokens[1])
	elif tokens[0] == "down":
		vpos += int(tokens[1])
	elif tokens[0] == "up":
		vpos -= int(tokens[1])
	else:
		print(f"Invalid tokens {tokens}")
		sys.exit(0)
	return hpos, vpos

def main():
	hpos = 0
	vpos = 0
	if len(sys.argv) < 2:
		sys.exit(0)
	with open(sys.argv[1], "r") as f:
		for line in f.readlines():
			hpos, vpos = modify_pos(hpos, vpos, line)
	print(hpos * vpos)

if __name__ == '__main__':
	main()
