#!/usr/bin/env python3

import sys
import unittest

class Tests(unittest.TestCase):
	def test_init_counts(self):
		self.assertEqual(get_init_counts("10101010"), [1, -1, 1, -1, 1, -1, 1, -1])
	
	def test_modify_counts(self):
		self.assertEqual(modify_counts("10101010", [1, 1, -1, -1, 0, 9, 10, -23]), [2, 0, 0, -2, 1, 8, 11, -24])

	def test_counts_to_gamma(self):
		self.assertEqual(counts_to_gamma_epsilon([4, 4, -5, -1, 5]), (25, 6))
		
def get_init_counts(line):
	"""Gets the initial counts for the first line only, as a list with indices representing the
	   number of 1s at the index - the number of 0s at the index"""
	counts = []
	for char in line:
		if char == "0":
			counts.append(-1)
		elif char == "1":
			counts.append(1)
		else:
			# error
			pass
	return counts

def modify_counts(line, counts):
	"""Modifies the counts list, using the binary string for a single line"""
	for idx, char in enumerate(line):
		if char == "0":
			counts[idx] -= 1
		elif char == "1":
			counts[idx] += 1
		else:
			# error
			pass
	return counts

def counts_to_gamma_epsilon(counts):
	"""Converts a list of counts to the corresponding gamma and epsilon values, creating a binary number
	   with digits of 0 if negative or 1 if positive, and its unsigned bitwise inverse"""
	gamma_str = ""
	epsilon_str = ""
	for count in counts:
		if count < 0:
			gamma_str += "0"
			epsilon_str += "1"
		else:
			gamma_str += "1"
			epsilon_str += "0"
	return int(gamma_str, 2), int(epsilon_str, 2)

def main():
	if len(sys.argv) < 2:
		sys.exit(0)
	with open(sys.argv[1], "r") as f:
		counts = get_init_counts(f.readline())
		for line in f.readlines():
			modify_counts(line, counts)
		#print(counts)
	gamma, epsilon = counts_to_gamma_epsilon(counts)
	print(gamma * epsilon)

if __name__ == '__main__':
	#unittest.main()
	main()
