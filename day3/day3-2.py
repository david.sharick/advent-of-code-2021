#!/usr/bin/env python3

import sys
import unittest

class Tests(unittest.TestCase):
	def test_filter_lines(self):
		self.assertEqual(filter_lines(["10", "01", "11"], 0, True), ["10", "11"])
		self.assertEqual(filter_lines(["10", "01", "11"], 0, False), ["01"])
		self.assertEqual(filter_lines(["10", "01", "11"], 1, True), ["01", "11"])

def filter_lines(lines, index, prefer_larger):
	"""Filters the lines by keeping only numbers with a specific value for their index-th bit,
	   either the more common or less common depending on the value of prefer_larger"""
	extra_1s = 0
	zeros_lines = []
	ones_lines = []
	for line in lines:
		if line[index] == "0":
			extra_1s -= 1
			zeros_lines.append(line)
		elif line[index] == "1":
			extra_1s += 1
			ones_lines.append(line)
		else:
			# error
			pass
	if extra_1s < 0:
		return zeros_lines if prefer_larger else ones_lines
	elif extra_1s >= 0:
		return ones_lines if prefer_larger else zeros_lines

def main():
	oxygen_lines = []
	co2_lines = []
	if len(sys.argv) < 2:
		sys.exit(0)
	with open(sys.argv[1], "r") as f:
		for line in f.readlines():
			oxygen_lines.append(line)
			co2_lines.append(line)
		i = 0
		j = 0
		while (len(oxygen_lines) > 1):
			oxygen_lines = filter_lines(oxygen_lines, i, True)
			i += 1
		while (len(co2_lines) > 1):
			co2_lines = filter_lines(co2_lines, j, False)
			j += 1
		o2_rating = int(oxygen_lines[0], 2)
		co2_rating = int(co2_lines[0], 2)
		print(o2_rating * co2_rating)

if __name__ == '__main__':
	#unittest.main()
	main()
