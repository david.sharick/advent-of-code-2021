#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import math
import string
import sys
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """target area: x=20..30, y=-10..-5"""
		self.test = parse_input(self.testinput)
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 45)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 112)
		pass

def parse_input(i):
	y_thing = [int(x) for x in i[i.rfind("=") + 1:].split("..")]
	x_thing = [int(x) for x in i[i.find("=") + 1:i.find(",")].split("..")]
	return ((x_thing[0], y_thing[0]), (x_thing[1], y_thing[1]))

def valid_vel_pair(y_0, x_0, y_min, y_max, x_min, x_max):
	"""Validates that the velocity will ever end up within the given bounds after a step"""
	steps = 0
	y_pos = 0
	x_pos = 0
	while y_pos >= y_min and x_pos <= x_max and (x_0 > 0 or x_pos >= x_min):
		 steps += 1
		 y_pos += y_0
		 x_pos += x_0
		 y_0 -= 1
		 x_0 = max(0, x_0 - 1)
		 if y_pos >= y_min and y_pos <= y_max and x_pos >= x_min and x_pos <= x_max:
		 	return True
	return False

def part1(i):
	y_min = i[0][1]
	y_0 = -(y_min + 1)
	return ((y_0 * y_0) + y_0) / 2

def part2(i):
	valid_count = 0
	y_min = i[0][1]
	y_max = i[1][1]
	x_min = i[0][0]
	x_max = i[1][0]
	for y in range(y_min, -(y_min + 1) + 1):
		for x in range(x_max + 1):
			if valid_vel_pair(y, x, y_min, y_max, x_min, x_max):
				valid_count += 1
	return valid_count

def main():
	if len(sys.argv) < 2:
		sys.exit(0)
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readline())
	print(part1(i))
	print(part2(i))

if __name__ == '__main__':
	#unittest.main()
	main()
