#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import math
import string
import sys
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """1163751742
1381373672
2136511328
3694931569
7463417111
1319128137
1359912421
3125421639
1293138521
2311944581"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 40)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 315)
		pass

def parse_input(i):
	out = []
	for line in i:
		out.append([int(c) for c in line if c != "\n"])
	return out

def part1(inp):
	opt = []
	for i in range(len(inp)):
		opt.append([math.inf for i in range(len(inp[0]))])
	opt[0][0] = inp[0][0]
	for j in range(1, len(inp[0])):
		opt[0][j] = inp[0][j] + opt[0][j - 1]
	for i in range(1, len(inp)):
		opt[i][0] = inp[i][0] + opt[i - 1][0]
		for j in range(1, len(inp[0])):
			opt[i][j] = inp[i][j] + min(opt[i - 1][j], opt[i][j - 1])
	return opt[len(inp) - 1][len(inp[0]) - 1] - inp[0][0]

def increment(val, amt):
	"""Increments a value by a specific amount, with 10 wrapping down to 1"""
	new_val = val + amt
	while new_val > 9:
		new_val -= 9
	return new_val

def fill_opt(y, x, max_y, max_x, opt, inp):
	"""Updates the opt table for the positions neighboring x, y"""
	neighbors = []
	if y > 0:
		neighbors.append((y - 1, x))
	if x > 0:
		neighbors.append((y, x - 1))
	if y < max_y:
		neighbors.append((y + 1, x))
	if x < max_x:
		neighbors.append((y, x + 1))
	for n in neighbors:
		opt[n[0]][n[1]] = min(opt[n[0]][n[1]], opt[y][x] + inp[n[0]][n[1]])
	return opt

def part2(inp):
	new_inp = []
	for i in range(len(inp) * 5):
		new_inp.append([0 for i in range(len(inp[0]) * 5)])
	x_size = len(inp[0])
	y_size = len(inp)
	visited = []
	for i in range(5):
		for j in range(5):
			# i + j = num incs
			# fill in
			for y in range(y_size):
				for x in range(x_size):
					new_inp[y + (y_size * j)][x + (x_size * i)] = increment(inp[y][x], i + j)
	opt = []
	for i in range(y_size * 5):
		opt.append([math.inf for i in range(x_size * 5)])
	opt[0][0] = new_inp[0][0]
	for i in range(10): # hacky way to ensure opt is fully calculated
		for i in range(0, len(new_inp)):
			for j in range(0, len(new_inp[0])):
				opt = fill_opt(i, j, (y_size * 5) - 1, (x_size * 5) - 1, opt, new_inp)
	return opt[len(new_inp) - 1][len(new_inp[0]) - 1] - new_inp[0][0]

def main():
	if len(sys.argv) < 2:
		sys.exit(0)
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	print(part2(i))

if __name__ == '__main__':
	unittest.main()
	main()
