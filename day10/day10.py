#!/usr/bin/env python3

import sys
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]""".split("\n")
		#self.test = 
	
	def test_part1(self):
		self.assertEqual(part1(self.testinput), 26397)
		
	def test_part2(self):
		self.assertEqual(part2(self.testinput), 288957)
	
	def test_corrupt_line_value(self):
		self.assertEqual(corrupt_line_value("{([(<{}[<>[]}>{[]{[(<()>"), 1197)
	
	def test_str_value(self):
		self.assertEqual(str_value("}}]])})]"), 288957)
	
	def test_completion_value(self):
		self.assertEqual(completion_value("[({(<(())[]>[[{[]{<()<>>"), 288957)
	

OPENERS = "<{[("
CLOSERS = ">}])"
PAIRS = {"<": ">", "{": "}", "[": "]", "(": ")"}
ILLEGALS = {")": 3, "]": 57, "}": 1197, ">": 25137}
COMPLETIONS = {")": 1, "]": 2, "}": 3, ">": 4}

def corrupt_line_value(line):
	"""Returns the value of a line, assuming the line is corrupt"""
	char_stack = []
	for char in line:
		if char in OPENERS:
			char_stack.append(char)
		elif char == "\n":
			return 0
		else: # is a closer
			last = char_stack.pop()
			if PAIRS[last] != char:
				return ILLEGALS[char]
	return 0

def str_value(chars):
	"""Returns the value of a list of openers that need corresponding closers"""
	running = 0
	for char in chars[::-1]:
		running *= 5
		running += COMPLETIONS[PAIRS[char]]
	return running

def completion_value(line):
	"""Returns the value of a line, assuming the line is incomplete"""
	char_stack = []
	for char in line:
		if char in OPENERS:
			char_stack.append(char)
		elif char == "\n":
			return str_value(char_stack)
		else: # is a closer
			last = char_stack.pop()
	return str_value(char_stack)

def part1(lines):
	return sum([corrupt_line_value(line) for line in lines])

def part2(lines):
	incompletes = [line for line in lines if corrupt_line_value(line) == 0]
	values = list([completion_value(line) for line in incompletes])
	length = len(values)
	values.sort()
	return values[(length - 1) // 2]

def main():
	if len(sys.argv) < 2:
		sys.exit(0)
	lines = []
	with open(sys.argv[1], "r") as f:
		for line in f.readlines():
			if line == "\n":
				break
			lines.append(line)
	print(part1(lines))
	print(part2(lines))

if __name__ == '__main__':
	#unittest.main()
	main()
