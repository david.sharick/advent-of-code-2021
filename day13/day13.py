#!/usr/bin/env python3

from collections import defaultdict
from functools import lru_cache
import sys
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 17)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), """#####
#...#
#...#
#...#
#####
.....
.....
""")
		pass

def parse_input(i):
	max_x = 0
	max_y = 0
	covered = []
	folds = []
	at_folds = False
	grid = []
	for line in i:
		if line == "\n" or line == "":
			at_folds = True
		elif at_folds:
			horiz = "x" in line # fold itself, not line being folded across
			coord = int(line.split("=")[1])
			folds.append((horiz, coord))
		else:
			pos = [int(p) for p in line.strip().split(",")]
			x, y = pos[0], pos[1]
			max_x = max(x, max_x)
			max_y = max(y, max_y)
			covered.append((y, x))
	for i in range(max_y + 1):
		grid.append([False for i in range(max_x + 1)])
	for pos in covered:
		grid[pos[0]][pos[1]] = True
	return (grid, folds)

def apply_fold(grid, fold):
	max_y = len(grid) - 1
	max_x = len(grid[0]) - 1
	# point x,y on grid is now [x][y] OR crosspoint:
	# if horiz: first check that 
	if fold[0]: # h case
		for y in range(max_y + 1):
			for x in range(fold[1]):
				curr = grid[y][x]
				diff = abs(fold[1] - x)
				new_x = fold[1] + diff
				if new_x > max_x:
					pass
				else:
					grid[y][x] = curr or grid[y][new_x]
		for i, l in enumerate(grid):
			grid[i] = l[:fold[1]]
		return grid
	else: # v case
		for y in range(fold[1]):
			for x in range(max_x + 1):
				curr = grid[y][x]
				diff = abs(fold[1] - y)
				new_y = fold[1] + diff
				if new_y > max_y:
					pass
				else:
					grid[y][x] = curr or grid[new_y][x]
		return grid[0:fold[1]]
		

def part1(i):
	fold = i[1][0]
	grid = apply_fold(i[0], fold)
	return sum(sum(1 if p else 0 for p in l) for l in grid)

def part2(i):
	grid = i[0]
	for f in i[1]:
		grid = apply_fold(grid, f)
	out = ""
	for l in grid:
		l = "".join("#" if p else "." for p in l)
		out += (l + "\n")
	return out

def main():
	if len(sys.argv) < 2:
		sys.exit(0)
	with open(sys.argv[1], "r") as f:
		i = parse_input(f)
	print(part1(i))
	with open(sys.argv[1], "r") as f:
		i = parse_input(f)
	print(part2(i))

if __name__ == '__main__':
	unittest.main()
	main()
