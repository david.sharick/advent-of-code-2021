#!/usr/bin/env python3

import sys
import unittest

class Tests(unittest.TestCase):
	def test_get_line_downright(self):
		self.assertTrue(get_line_downright([[5, 5], [2, 2]]))
		self.assertFalse(get_line_downright([[5, 5], [8, 2]]))

def get_line_downright(line):
	"""Returns whether a diagonal line points down-right or not, where down means increasing y
	   with increasing x"""
	backwards = line[0][0] > line[1][0]
	down = line[0][1] < line[1][1]
	return backwards ^ down

def main():
	if len(sys.argv) < 2:
		sys.exit(0)
	horz = []
	vert = []
	diag = []
	with open(sys.argv[1], "r") as f:
		for line in f.readlines():
			if line == "\n":
				break
			positions = line.split(" -> ")
			values = [list(map(int, pos.split(","))) for pos in positions]
			if values[0][0] == values[1][0]:
				vert.append(values)
			elif values[0][1] == values[1][1]:
				horz.append(values)
			else:
				diag.append(values)
	
	count = 0
	positions = [[0 for i in range(1000)] for i in range(1000)]
	
	for line in horz:
		y = line[0][1]
		start = min(line[0][0], line[1][0])
		end = max(line[0][0], line[1][0])
		for x in range(start, end + 1):
			positions[y][x] += 1
	
	for line in vert:
		x = line[0][0]
		start = min(line[0][1], line[1][1])
		end = max(line[0][1], line[1][1])
		for y in range(start, end + 1):
			positions[y][x] += 1
	
	for line in diag:
		start_x = min(line[0][0], line[1][0])
		end_x = max(line[0][0], line[1][0])
		is_down_right = get_line_downright(line)
		start_y = line[0][1] if start_x == line[0][0] else line[1][1]
		for i in range(0, (end_x - start_x) + 1):
			y = start_y + i if is_down_right else start_y - i
			positions[y][start_x + i] += 1
	
	for row in positions:
		for pos in row:
			if pos > 1:
				count += 1
	print(count)

if __name__ == '__main__':
	#unittest.main()
	main()
