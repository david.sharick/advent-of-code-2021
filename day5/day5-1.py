#!/usr/bin/env python3

import sys

def main():
	if len(sys.argv) < 2:
		sys.exit(0)
	horz = []
	vert = []
	with open(sys.argv[1], "r") as f:
		for line in f.readlines():
			if line == "\n":
				break
			positions = line.split(" -> ")
			values = [list(map(int, pos.split(","))) for pos in positions]
			if values[0][0] == values[1][0]:
				vert.append(values)
			elif values[0][1] == values[1][1]:
				horz.append(values)
	
	count = 0
	positions = [[0 for i in range(1000)] for i in range(1000)]
	
	for line in horz:
		y = line[0][1]
		start = min(line[0][0], line[1][0])
		end = max(line[0][0], line[1][0])
		for x in range(start, end + 1):
			positions[x][y] += 1
	for line in vert:
		x = line[0][0]
		start = min(line[0][1], line[1][1])
		end = max(line[0][1], line[1][1])
		for y in range(start, end + 1):
			positions[x][y] += 1
	
	for row in positions:
		for pos in row:
			if pos > 1:
				count += 1
	print(count)

if __name__ == '__main__':
	main()
