#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """--- scanner 0 ---
404,-588,-901
528,-643,409
-838,591,734
390,-675,-793
-537,-823,-458
-485,-357,347
-345,-311,381
-661,-816,-575
-876,649,763
-618,-824,-621
553,345,-567
474,580,667
-447,-329,318
-584,868,-557
544,-627,-890
564,392,-477
455,729,728
-892,524,684
-689,845,-530
423,-701,434
7,-33,-71
630,319,-379
443,580,662
-789,900,-551
459,-707,401

--- scanner 1 ---
686,422,578
605,423,415
515,917,-361
-336,658,858
95,138,22
-476,619,847
-340,-569,-846
567,-361,727
-460,603,-452
669,-402,600
729,430,532
-500,-761,534
-322,571,750
-466,-666,-811
-429,-592,574
-355,545,-477
703,-491,-529
-328,-685,520
413,935,-424
-391,539,-444
586,-435,557
-364,-763,-893
807,-499,-711
755,-354,-619
553,889,-390

--- scanner 2 ---
649,640,665
682,-795,504
-784,533,-524
-644,584,-595
-588,-843,648
-30,6,44
-674,560,763
500,723,-460
609,671,-379
-555,-800,653
-675,-892,-343
697,-426,-610
578,704,681
493,664,-388
-671,-858,530
-667,343,800
571,-461,-707
-138,-166,112
-889,563,-600
646,-828,498
640,759,510
-630,509,768
-681,-892,-333
673,-379,-804
-742,-814,-386
577,-820,562

--- scanner 3 ---
-589,542,597
605,-692,669
-500,565,-823
-660,373,557
-458,-679,-417
-488,449,543
-626,468,-788
338,-750,-386
528,-832,-391
562,-778,733
-938,-730,414
543,643,-506
-524,371,-870
407,773,750
-104,29,83
378,-903,-323
-778,-728,485
426,699,580
-438,-605,-362
-469,-447,-387
509,732,623
647,635,-688
-868,-804,481
614,-800,639
595,780,-596

--- scanner 4 ---
727,592,562
-293,-554,779
441,611,-461
-714,465,-776
-743,427,-804
-660,-479,-426
832,-632,460
927,-485,-438
408,393,-506
466,436,-512
110,16,151
-258,-428,682
-393,719,612
-211,-452,876
808,-476,-593
-575,615,604
-485,667,467
-680,325,-822
-627,-443,-432
872,-547,-609
833,512,582
807,604,487
839,-516,451
891,-625,532
-652,-548,-490
30,-46,-14"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 79)
		#self.assertTrue(has_12_pairs(self.test[0], self.test[1]))
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 3621)
		pass


def parse_input(i):
	out = {}
	curr = 0
	for line in i:
		if line == "\n":
			continue
		l = [int(s) for s in line.split() if s.isdigit()]
		if len(l) == 1:
			curr = l[0]
			out[curr] = []
		else:
			try:
				out[curr].append([int(c) for c in line.split(",")])
			except:
				pass
	#print(out)
	return out


def apply_orientation(dist, orie):
	out = list(list(itertools.permutations(dist))[orie[0]])
	if orie[1] & 1:
		out[2] *= -1
	if orie[1] & 2:
		out[1] *= -1
	if orie[1] & 4:
		out[0] *= -1
	return out


def has_12_pairs(scan1, scan2, swap = False):
	sames = []
	for i in range(48): #every orientation
		orie = (i // 8, i % 8)
		#print(orie)
		# if we apply the orientation to all dists in dists_2/dists_1, we can use that to check for 12 matches
		new_dists = [apply_orientation(dist, orie) for dist in scan2] if not swap else [apply_orientation(dist, orie) for dist in scan1]
		# we need to then see if 12 dists all share the same relative offsets: offset from s1 to s2 (which we don't actually know)
		thing = []
		if not swap:
			for pr in itertools.product(scan1, new_dists):
				new = ((pr[0][0] - pr[1][0], pr[0][1] - pr[1][1], pr[0][2] - pr[1][2]))
				thing.append(new)
		else:
			for pr in itertools.product(scan2, new_dists):
				new = ((pr[0][0] - pr[1][0], pr[0][1] - pr[1][1], pr[0][2] - pr[1][2]))
				thing.append(new)
		c = Counter(thing)
		for k, v in c.items():
			if v > 11:
				return True, (k, orie)
	return False, ()
	
def merge(s1, s2, rel):
	for pos in s2:
		reor = apply_orientation(pos, rel[1])
		new = [reor[i] + rel[0][i] for i in range(3)]
		if new not in s1:
			s1.append(new)
	return s1

def part1(i):
	weird_dict = {}
	zeros_or = set()
	zeros_or.add(0)
	for x in i.keys():
		for y in i.keys():
			if x == y or (y, x) in weird_dict.keys() or len(i[x]) == 0 or len(i[y]) == 0:
				pass
			else:
				valid, rel = has_12_pairs(i[x], i[y], not x in zeros_or)
				if x in zeros_or:
					zeros_or.add(y)
				if valid:
					weird_dict[(x, y)] = rel
					# we can use this to merge the two
					new = merge(i[x], i[y], rel)
					i[x] = new
					i[y] = []
	return sum(len(v) for k, v in i.items())

def part2(i):
	weird_dict = {}
	zeros_or = set()
	zeros_or.add(0)
	for x in [0, 0, 0, 0, 0]:
		for y in i.keys():
			if x == y or (y, x) in weird_dict.keys() or len(i[x]) == 0 or len(i[y]) == 0:
				pass
			else:
				valid, rel = has_12_pairs(i[x], i[y], not x in zeros_or)
				if x in zeros_or:
					zeros_or.add(y)
				if valid:
					weird_dict[(x, y)] = rel
					# we can use this to merge the two
					new = merge(i[x], i[y], rel)
					i[x] = new
					i[y] = []
	offset_dict = {0: [0, 0, 0]}
	for i in range(1):
		for k, v in weird_dict.items():
			if k[0] == 0:
				offset_dict[k[1]] = v[0]
			elif k[1] == 0:
				offset_dict[k[0]] = [-x for x in v[0]]
			else:
				# try to recursive fill in
				if k[0] in offset_dict.keys() and k[1] not in offset_dict.keys():
					offset_dict[k[1]] = [offset_dict[k[0]][i] + v[0][i] for i in range(3)]
				elif k[1] in offset_dict.keys() and k[0] not in offset_dict.keys():
					offset_dict[k[0]] = [offset_dict[k[1]][i] + v[0][i] for i in range(3)]
	l = [v for k, v in offset_dict.items()]
	mmd = 0
	for x in range(len(l)):
		for y in range(len(l)):
			if x == y:
				pass
			md = abs(l[x][0] - l[y][0]) + abs(l[x][1] - l[y][1]) + abs(l[x][2] - l[y][2])
			mmd = max(md, mmd)
	return mmd

def main():
	if len(sys.argv) < 2:
		sys.exit(0)
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))

if __name__ == '__main__':
	#unittest.main()
	main()
