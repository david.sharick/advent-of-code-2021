#!/usr/bin/env python3

from collections import defaultdict
import sys
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """dc-end
HN-start
start-kj
dc-start
dc-HN
LN-dc
HN-end
kj-sa
kj-HN
kj-dc"""
		self.si = """start-A
start-b
A-c
A-b
b-d
A-end
b-end"""
		self.testg = [set(), defaultdict(list)]
		for line in self.testinput.split("\n"):
			parts = [p.strip() for p in line.split("-")]
			self.testg[0].add(parts[0])
			self.testg[0].add(parts[1])
			self.testg[1][parts[0]].append(parts[1])
			self.testg[1][parts[1]].append(parts[0])
		self.testg2 = [set(), defaultdict(list)]
		for line in self.si.split("\n"):
			parts = [p.strip() for p in line.split("-")]
			self.testg2[0].add(parts[0])
			self.testg2[0].add(parts[1])
			self.testg2[1][parts[0]].append(parts[1])
			self.testg2[1][parts[1]].append(parts[0])
	
	def test_part1(self):
		self.assertEqual(part1(self.testg), 19)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.testg2), 36)
		pass

class Tree:
	def __init__(self, name, path, graph, explored):
		# inits recursively
		self.name = name
		self.path = path
		self.children = []
		self.explored = explored.copy()
		if self.name != "end":
			if not self.name.isupper():
				self.explored.add(self.name)
			for i, n in enumerate(graph[1][name]):
				if n not in explored and n != "end":
					self.children.append(Tree(n, self.path + [i], graph, self.explored))
				elif n == "end":
					# we found a path
					self.children.append("end")
	
	def total_ends(self):
		"""Calculates the total number of end nodes in the tree"""
		s = 0
		for c in self.children:
			if c == "end":
				s += 1
			else:
				s += c.total_ends()
		return s

	def __str__(self):
		return f"{self.name}: path {self.path} kids \n[".join([str(c) for c in self.children])

class ExtTree:
	def __init__(self, name, path, graph, explored, doubled):
		# inits recursively
		self.name = name
		self.path = path
		self.children = []
		self.explored = explored.copy()
		if self.name != "end":
			if not self.name.isupper():
				self.explored.add(self.name)
			for i, n in enumerate(graph[1][name]):
				if n not in explored and n != "end":
					self.children.append(ExtTree(n, self.path + [i], graph, self.explored, doubled))
				elif n == "end":
					# we found a path
					self.children.append("end")
				elif n in explored and not doubled and n != "start":
					self.children.append(ExtTree(n, self.path + [i], graph, self.explored, True))
	
	def total_ends(self):
		"""Calculates the total number of end nodes in this tree"""
		s = 0
		for c in self.children:
			if c == "end":
				s += 1
			else:
				s += c.total_ends()
		return s

	def __str__(self):
		out = f"{self.name}: path {self.path} kids \n[".join([str(c) for c in self.children])
		return out + "]"
		

def part1(g):
	t = Tree("start", [0], g, set())
	return t.total_ends()

def part2(g):
	t = ExtTree("start", [0], g, set(), False)
	return t.total_ends()

def main():
	if len(sys.argv) < 2:
		sys.exit(0)
	graph = [set(), defaultdict(list)]
	with open(sys.argv[1], "r") as f:
		for line in f.readlines():
			if line == "\n":
				break
			parts = [p.strip() for p in line.split("-")]
			graph[0].add(parts[0])
			graph[0].add(parts[1])
			graph[1][parts[0]].append(parts[1])
			graph[1][parts[1]].append(parts[0])
	print(part1(graph))
	print(part2(graph))

if __name__ == '__main__':
	unittest.main()
	main()
