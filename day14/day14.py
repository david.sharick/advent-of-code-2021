#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import math
import string
import sys
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 1588)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 2188189693529)
		pass

def parse_input(i):
	rules = {}
	for i, line in enumerate(i):
		if i == 0:
			chem = line.strip()
		elif i == 1:
			pass
		else:
			parts = [p.strip() for p in line.split("->")]
			rules[parts[0]] = parts[1]
	return (chem, rules)

def apply_rules(chem, rules):
	"""Applies the rules to a chemical string once, giving the new string"""
	out = chem[0]
	for i in range(len(chem) - 1):
		pair = chem[i:i + 2]
		try:
			out += rules[pair] + pair[1]
		except:
			out += pair[1]
	return out


def apply_rules_opt(chem, rules):
	"""Applies the rules to a chemical counter of pairs, giving the new counts"""
	new_count = chem.copy()
	for k, v in chem.items():
		try:
			new = rules[k]
			new_count[k] -= v
			new_count[k[0] + new] += v
			new_count[new + k[1]] += v
		except:
			new_count[k] = v
	return new_count
			

def part1(i):
	c = i[0]
	r = i[1]
	for i in range(10):
		c = apply_rules(c, r)
	char_count = Counter(c)
	min_v = math.inf
	max_v = 0
	for k, v in char_count.items():
		if v < min_v:
			min_v = v
		if v > max_v:
			max_v = v
	return max_v - min_v


def part2(i):
	c = i[0]
	r = i[1]
	last_char = c[-1]
	c_count = Counter()
	for i in range(len(c) - 1):
		c_count[c[i:i + 2]] += 1
	for i in range(40):
		c_count = apply_rules_opt(c_count, r)
	char_count = Counter(string.ascii_uppercase)
	for k in char_count.keys():
		char_count[k] = 0
	for k, v in c_count.items():
		char = k[0]
		char_count[char] += v
	char_count[last_char] += 1
	min_v = math.inf
	max_v = 0
	for k, v in char_count.items():
		if v < min_v and v > 0:
			min_v = v
		if v > max_v:
			max_v = v
	return max_v - min_v

def main():
	if len(sys.argv) < 2:
		sys.exit(0)
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	print(part2(i))

if __name__ == '__main__':
	#unittest.main()
	main()
