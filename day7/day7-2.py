#!/usr/bin/env python3

import sys
import unittest

class Tests(unittest.TestCase):
	def test_smallest_sum_of_squared_diffs(self):
		self.assertEqual(get_smallest_sum_of_squared_diffs([16,1,2,0,4,2,7,1,2,14]), 5)
	
	def test_cheapest_fuel(self):
		self.assertEqual(get_cheapest_fuel([16,1,2,0,4,2,7,1,2,14]), 168)

def get_cheapest_fuel(crabs):
	"""Gets the cheapest amount of fuel the crabs could possibly use to align to the same horizontal position"""
	new_pos = get_smallest_sum_of_squared_diffs(crabs)
	out = 0
	for crab in crabs:
		dist = abs(new_pos - crab)
		fuel = ((dist * dist) + dist) / 2
		out += fuel
	return out

# smallest sum of differences
def get_smallest_sum_of_squared_diffs(crabs):
	"""Calculates the integer whose sum of squared differences to the given list is smallest"""
	base_crab = round(sum(crabs) // len(crabs))
	print(base_crab)
	return base_crab

def main():
	if len(sys.argv) < 2:
		sys.exit(0)
	with open(sys.argv[1], "r") as f:
		for line in f.readlines():
			if line == "\n":
				break
			crabs = [int(c) for c in line.split(",")]
	print(get_cheapest_fuel(crabs))

if __name__ == '__main__':
	#unittest.main()
	main()
