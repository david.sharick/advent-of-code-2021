#!/usr/bin/env python3

import sys
import unittest

class Tests(unittest.TestCase):
	def test_smallest_sum_of_diffs(self):
		self.assertEqual(get_smallest_sum_of_diffs([16,1,2,0,4,2,7,1,2,14]), 2)
	
	def test_cheapest_fuel(self):
		self.assertEqual(get_cheapest_fuel([16,1,2,0,4,2,7,1,2,14]), 37)

def get_cheapest_fuel(crabs):
	"""Gets the cheapest amount of fuel the crabs could possibly use to align to the same horizontal position"""
	new_pos = get_smallest_sum_of_diffs(crabs)
	out = 0
	for crab in crabs:
		out += abs(new_pos - crab)
	return out

# smallest sum of differences
def get_smallest_sum_of_diffs(crabs):
	"""Calculates the integer whose sum of differences to the given list is smallest"""
	crabs.sort()
	base_crab = crabs[len(crabs) // 2] # median
	return base_crab

def main():
	if len(sys.argv) < 2:
		sys.exit(0)
	with open(sys.argv[1], "r") as f:
		for line in f.readlines():
			if line == "\n":
				break
			crabs = [int(c) for c in line.split(",")]
	print(get_cheapest_fuel(crabs))

if __name__ == '__main__':
	#unittest.main()
	main()
