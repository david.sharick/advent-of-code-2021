#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import math
import string
import sys
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#

#..#.
#....
##..#
..#..
..###"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 35)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 3351)
		pass

def parse_input(i):
	grid = []
	for idx, line in enumerate(i):
		if idx == 0:
			alg = list([c == "#" for c in line])
		elif idx > 1:
			grid.append([c == "#" for c in line.strip()])
	return (grid, alg)

def get_bit(x, y, img, default):
	"""Gets the value at position x,y in the image"""
	try:
		return img[y][x]
	except:
		return default # the oob value

def apply_alg(img, x, y, alg, default):
	"""Applies the enhancement algorithm to the pixel at x,y in img"""
	bits = []
	bits.append(get_bit(x - 1, y - 1, img, default))
	bits.append(get_bit(x, y - 1, img, default))
	bits.append(get_bit(x + 1, y - 1, img, default))
	bits.append(get_bit(x - 1, y, img, default))
	bits.append(get_bit(x, y, img, default))
	bits.append(get_bit(x + 1, y, img, default))
	bits.append(get_bit(x - 1, y + 1, img, default))
	bits.append(get_bit(x, y + 1, img, default))
	bits.append(get_bit(x + 1, y + 1, img, default))
	# convert to int
	val = sum([2 ** i if bits[8 - i] else 0 for i in range(9)])
	return alg[val]

def enhance_image(img, alg, default):
	"""Applies the enhancement algorithm to the image once"""
	# we extend out by 1 in all dirs, everything beyond that is an infinite expanse of the same pixel
	out = [[False for i in range(len(img[0]) + 2)] for i in range(len(img) + 2)]
	img.append([default for _ in range(len(img[0]))])
	img.insert(0, [default for _ in range(len(img[0]))])
	for l in img:
		l.insert(0, default)
		l.append(default)
	for y in range(len(out)):
		for x in range(len(out[y])):
			new_pixel = apply_alg(img, x, y, alg, default)
			out[y][x] = new_pixel
	default = alg[-1] if default else alg[0]
	return out, default

def count_lit(grid):
	"""Counts the number of lit pixels in a grid"""
	out = 0
	for y in range(len(grid)):
		for x in range(len(grid[y])):
			out += 1 if grid[y][x] else 0
	return out

def pretty_print(grid):
	"""Pretty-prints a grid of pixels"""
	for y in range(len(grid)):
		print("".join(["#" if c else "." for c in grid[y]]))

def part1(i):
	default = False
	end, default = enhance_image(i[0], i[1], default)
	end, default = enhance_image(end, i[1], default)
	return count_lit(end)

def part2(i):
	default = False
	end = i[0]
	for _ in range(50):
		end, default = enhance_image(end, i[1], default)
	return count_lit(end)

def main():
	if len(sys.argv) < 2:
		sys.exit(0)
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))

if __name__ == '__main__':
	#unittest.main()
	main()
