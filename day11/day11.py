#!/usr/bin/env python3

import itertools
import sys
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.maxDiff = None
		self.testinput = """5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526"""
		self.test = [[int(c) for c in list(line)] for line in self.testinput.split("\n")]
		self.once = """6594254334
3856965822
6375667284
7252447257
7468496589
5278635756
3287952832
7993992245
5957959665
6394862637"""
		self.twice = """8807476555
5089087054
8597889608
8485769600
8700908800
6600088989
6800005943
0000007456
9000000876
8700006848"""
		self.oncelines = [[int(c) for c in list(line)] for line in self.once.split("\n")]
		self.twicelines = [[int(c) for c in list(line)] for line in self.twice.split("\n")]
	
	def test_part1(self):
		self.assertEqual(part1(self.test, 10), 204)
		self.test = [[int(c) for c in list(line)] for line in self.testinput.split("\n")]
		self.assertEqual(part1(self.test, 100), 1656)
		pass
	
	def test_step_once(self):
		self.assertEqual(step_once(self.test), (self.oncelines, 0))
		self.assertEqual(step_once(self.oncelines), (self.twicelines, 35))
		
	def test_part2(self):
		self.test = [[int(c) for c in list(line)] for line in self.testinput.split("\n")]
		self.assertEqual(part2(self.test), 195)
		pass
	
	def test_get_adj(self):
		self.assertEqual(list(get_adj(0, 0, self.test)), [(0, 0), (0, 1), (1, 0), (1, 1)])
		self.assertEqual(list(get_adj(5, 5, self.test)), [(5, 5), (5, 4), (5, 6), (4, 5), (4, 4), (4, 6), (6, 5), (6, 4), (6, 6)])
	
def get_adj(y, x, lines):
	adj_y = [y]
	adj_x = [x]
	if y > 0:
		adj_y.append(y - 1)
	#else:
	#	adj_y.append(len(lines) - 1)
	if x > 0:
		adj_x.append(x - 1)
	#else:
	#	adj_x.append(len(lines[0]) - 1)
	if y < len(lines) - 1:
		adj_y.append(y + 1)
	#else:
	#	adj_y.append(0)
	if x < len(lines[0]) - 1:
		adj_x.append(x + 1)
	#else:
	#	adj_x.append(0)
	return itertools.product(adj_y, adj_x)

def step_once(lines):
	stk = [] # contains positions that need to increase due to a flash
	flashed = set()
	for y, row in enumerate(lines):
		for x, o in enumerate(row):
			lines[y][x] += 1
	
	for y, row in enumerate(lines):
		for x, o in enumerate(row):
			if o > 9:
				flashed.add((y, x))
				for pos in get_adj(y, x, lines):
					if (pos[0] != y or pos[1] != x) and pos not in flashed:
						stk.append((pos[0], pos[1]))
	
	while len(stk) > 0:
		pos = stk.pop()
		val = lines[pos[0]][pos[1]]
		if val > 8 and pos not in flashed:
			y, x = pos[0], pos[1]
			flashed.add((pos[0], pos[1]))
			for pos in get_adj(y, x, lines):
				if (pos[0] != y or pos[1] != x) and pos not in flashed:
					stk.append((pos[0], pos[1]))
		else:
			lines[pos[0]][pos[1]] += 1
	
	l = len(flashed)
	for pos in flashed:
		lines[pos[0]][pos[1]] = 0
	return lines, l
	

def part1(lines, steps):
	o = 0
	for i in range(steps):
		lines, x = step_once(lines)
		o += x
	return o
			

def part2(lines):
	i = 0
	while(True):
		i += 1
		lines, x = step_once(lines)
		if x == 100:
			break
	return i
		

def main():
	if len(sys.argv) < 2:
		sys.exit(0)
	lines = []
	with open(sys.argv[1], "r") as f:
		for line in f.readlines():
			if line == "\n":
				break
			lines.append([int(c) for c in list(line)[:-1]])
	print(part1(lines, 100))
	lines = []
	with open(sys.argv[1], "r") as f:
		for line in f.readlines():
			if line == "\n":
				break
			lines.append([int(c) for c in list(line)[:-1]])
	print(part2(lines))

if __name__ == '__main__':
	unittest.main()
	main()
