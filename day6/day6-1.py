#!/usr/bin/env python3

import sys
import unittest

CYCLE_RESET = 6
INIT_AGE = 8
NUM_DAYS = 80


class Tests(unittest.TestCase):
	def test_update_counts(self):
		self.assertEqual(update_counts([1, 1, 2, 1, 0, 0, 0, 0, 0]), [1, 2, 1, 0, 0, 0, 1, 0, 1])
	
	def test_get_final_state(self):
		self.assertEqual(get_final_state([0, 1, 1, 2, 1, 0, 0, 0, 0], 18), [3, 5, 3, 2, 2, 1, 5, 1, 4])

def update_counts(fish_counts):
	"""Updates the count of fish by decreasing the ages of all fish
	   and spawning new fish from any at age 0"""
	# save # of fish at 0
	# shift all down by 1
	# set 8 to num of 0s (because 0s spawn more)
	# incr 6s by 0s
	spawners = fish_counts[0]
	fish_counts = fish_counts[1:]
	fish_counts[6] += spawners
	fish_counts.append(spawners)
	return fish_counts

def get_final_state(fish_counts, days):
	"""Gets the final counts of fish after some amount of days, given an initial state of fish"""
	for i in range(days):
		fish_counts = update_counts(fish_counts)
	return fish_counts

def main():
	if len(sys.argv) < 2:
		sys.exit(0)
	fish_counts = [0 for i in range(INIT_AGE + 1)]
	with open(sys.argv[1], "r") as f:
		for line in f.readlines():
			if line == "\n":
				break
			fish = line.split(",")
			for f in fish:
				fish_counts[int(f)] += 1
	
	print(sum(get_final_state(fish_counts, NUM_DAYS)))

if __name__ == '__main__':
	#unittest.main()
	main()
