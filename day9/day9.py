#!/usr/bin/env python3

import sys
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """2199943210
3987894921
9856789892
8767896789
9899965678"""
		self.testmap = [[int(c) for c in list(line)] for line in self.testinput.split("\n")]
		
	def test_is_low_point(self):
		self.assertTrue(is_low_point(0, 9, self.testmap))
	
	def test_part1(self):
		self.assertEqual(part1(self.testmap), 15)
	
	def test_get_all_basins(self):
		print(get_all_basins(self.testmap))
	
	def test_get_neighbors(self):
		self.assertEqual(get_neighbors(0, 0, self.testmap), [((1, 0), 3), ((0, 1), 1)])
	
	def test_get_all_basins(self):
		self.assertEqual(get_all_basins(self.testmap), {(0, 1): {(0, 1), (0, 0), (1, 0)}, (0, 9): {(0, 7), (1, 8), (0, 9), (0, 6), (2, 9), (0, 5), (1, 6), (0, 8), (1, 9)}, (2, 2): {(2, 4), (1, 2), (2, 1), (3, 4), (4, 1), (3, 1), (1, 4), (3, 0), (2, 3), (3, 3), (2, 2), (3, 2), (2, 5), (1, 3)}, (4, 6): {(3, 8), (2, 7), (4, 9), (3, 7), (4, 6), (4, 5), (4, 8), (3, 6), (4, 7)}})
	
	def test_part2(self):
		self.assertEqual(part2(self.testmap), 1134)

def get_neighbors(y, x, hmap):
	"""Returns the neighbors of the specified point, as a list containing both coord tuples and values"""
	neighbors = []
	if y > 0:
		neighbors.append(((y - 1, x), hmap[y - 1][x]))
	if y < len(hmap) - 1:
		neighbors.append(((y + 1, x), hmap[y + 1][x]))
	if x > 0:
		neighbors.append(((y, x - 1), hmap[y][x - 1]))
	if x < len(hmap[0]) - 1:
		neighbors.append(((y, x + 1), hmap[y][x + 1]))
	return neighbors

def is_low_point(y, x, hmap):
	"""Returns whether the given point is a low point"""
	neighbors = []
	if y > 0:
		neighbors.append(hmap[y - 1][x])
	if y < len(hmap) - 1:
		neighbors.append(hmap[y + 1][x])
	if x > 0:
		neighbors.append(hmap[y][x - 1])
	if x < len(hmap[0]) - 1:
		neighbors.append(hmap[y][x + 1])
	our_val = hmap[y][x]
	return all([our_val < n for n in neighbors])

def get_all_basins(hmap):
	"""Splits a heightmap into a dict of basins"""
	low_points = []
	basins = {}
	basin_dict = {}
	for y in range(len(hmap)):
		for x in range(len(hmap[0])):
			if is_low_point(y, x, hmap):
				low_points.append((y, x))
				basins[(y, x)] = (y, x)
			elif hmap[y][x] == 9:
				pass
			else:
				# we want to create a basin num chain, set this to lowest neighbor
				#UNION FIND
				neighbors = get_neighbors(y, x, hmap)
				#print(neighbors)
				neighbors.sort(key=lambda k: k[1])
				basins[(y, x)] = neighbors[0][0] # set to lowest neighbor, this is guaranteed lower because not using low points here
	curr_basin = set()
	for y in range(len(hmap)):
		for x in range(len(hmap[0])):
			k = (y, x)
			if hmap[y][x] == 9:
				pass
			else:
				curr_basin = set()
				while True:
					if basins[k] == k:
						curr_basin.add(k)
						try:
							basin_dict[k].update(curr_basin)
						except:
							basin_dict[k] = curr_basin
						break
					else:
						curr_basin.add(k)
						k = basins[k]
	return basin_dict
				

def part1(hmap):
	out = 0
	for y in range(len(hmap)):
		for x in range(len(hmap[0])):
			if is_low_point(y, x, hmap):
				out += (1 + hmap[y][x])
	return out

def part2(hmap):
	basins = get_all_basins(hmap)
	basins_list = sorted(basins.values(), key = lambda b: len(b))
	#print(basins_list)
	return len(basins_list[-1]) * len(basins_list[-2]) * len(basins_list[-3])
	# get all basins: how?
	# take each low point, the (x, y) pair identifies the basin
	# starting from an arbitrary non-9, keep going down until finding a low point
	# use dict of xy tuple -> basin num
	# then take largest 3, this is easy

def main():
	if len(sys.argv) < 2:
		sys.exit(0)
	hmap = []
	with open(sys.argv[1], "r") as f:
		for line in f.readlines():
			if line == "\n":
				break
			hmap.append([int(c) for c in list(line)[:-1]])
	print(part1(hmap))
	print(part2(hmap))

if __name__ == '__main__':
	#unittest.main()
	main()
