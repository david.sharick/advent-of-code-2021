#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import math
import string
import sys
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """A0016C880162017C3686B18A3D4780"""
		self.test = parse_input(self.testinput)
		self.testinput2 = """9C0141080250320F1802104A08"""
		self.test2 = parse_input(self.testinput2)
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 31)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 1)
		pass

hex_to_bin = {"0": "0000", "1": "0001", "2": "0010", "3": "0011", "4": "0100", "5": "0101", "6": "0110", "7": "0111", "8": "1000", "9": "1001", "A": "1010", "B": "1011", "C": "1100", "D": "1101", "E": "1110", "F": "1111"}

class Packet:
	def __init__(self, p):
		self.p = p
		self.ver_num = int(p[:3], 2)
		self.type_id = int(p[3:6], 2)
		self.packet_length = 0
		if self.type_id == 4:
			i = 6
			val = ""
			while True:
				next_bits = p[i:i + 5]
				while len(next_bits) < 5:
					next_bits += "0"
				i += 5
				val += next_bits[1:]
				if next_bits[0] == "0":
					break
			self.val = int(val, 2)
			self.packet_length = i
		else:
			ltid = int(p[6])
			if ltid == 0:
				subpacket_len = int(p[7:22], 2)
				packets = p[22:22 + subpacket_len]
				self.packet_length = 22 + subpacket_len
				self.subpackets = []
				idx = 22
				while True:
					if idx >= self.packet_length:
						break
					self.subpackets.append(Packet(p[idx:]))
					idx += self.subpackets[-1].packet_length
			elif ltid == 1:
				subpacket_count = int(p[7:18], 2)
				i = 0
				idx = 18
				self.subpackets = []
				while i < subpacket_count:
					self.subpackets.append(Packet(p[idx:]))
					# need to figure out where to start
					idx += self.subpackets[-1].packet_length
					i += 1
				self.packet_length = idx
	
	def get_total_ver_num(self):
		if self.type_id == 4:
			return self.ver_num
		else:
			return self.ver_num + sum([p.get_total_ver_num() for p in self.subpackets])
	
	def get_value(self):
		if self.type_id == 0:
			return sum([p.get_value() for p in self.subpackets])
		if self.type_id == 1:
			v = 1
			for p in self.subpackets:
				v *= p.get_value()
			return v
		elif self.type_id == 2:
			return min([p.get_value() for p in self.subpackets])
		elif self.type_id == 3:
			return max([p.get_value() for p in self.subpackets])
		elif self.type_id == 4:
			return self.val
		elif self.type_id == 5:
			return 1 if self.subpackets[0].get_value() > self.subpackets[1].get_value() else 0
		elif self.type_id == 6:
			return 1 if self.subpackets[0].get_value() < self.subpackets[1].get_value() else 0
		elif self.type_id == 7:
			return 1 if self.subpackets[0].get_value() == self.subpackets[1].get_value() else 0

def parse_input(i):
	out = ""
	for c in i:
		if c != "\n":
			out += hex_to_bin[c]
	return out.rstrip("0")


def part1(i):
	pkt = Packet(i)
	return pkt.get_total_ver_num()	

def part2(i):
	pkt = Packet(i)
	return pkt.get_value()

def main():
	if len(sys.argv) < 2:
		sys.exit(0)
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readline())
	print(part1(i))
	print(part2(i))

if __name__ == '__main__':
	#unittest.main()
	main()
