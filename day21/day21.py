#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1([4, 8]), 739785)
		pass
		
	def test_part2(self):
		self.assertEqual(part2([4,8]), 444356092776315)
		pass

def parse_input(i):
	out = []
	for line in i:
		out.append(int(line[line.rfind(":") + 2:].strip()))
	return out

def roll_thrice(start):
	r1 = start + 1
	if r1 > 100:
		r1 -= 100
	r2 = start + 2
	if r2 > 100:
		r2 -= 100
	r3 = start + 3
	if r3 > 100:
		r3 -= 100
	start += 3
	if start > 100:
		start -= 100
	return [r1, r2, r3], start

def part1(i):
	p1_pos = i[0]
	p2_pos = i[1]
	p1_score = 0
	p2_score = 0
	rolls_count = 0
	die_val = 100
	while True:
		rolls, die_val = roll_thrice(die_val)
		p1_pos = (((p1_pos - 1) + sum(rolls)) % 10) + 1
		rolls_count += 3
		p1_score += p1_pos
		if p1_score >= 1000:
			return rolls_count * p2_score
		rolls, die_val = roll_thrice(die_val)
		p2_pos = (((p2_pos - 1) + sum(rolls)) % 10) + 1
		rolls_count += 3
		p2_score += p2_pos
		if p2_score >= 1000:
			return rolls_count * p1_score



def run_many_worlds(p1_score, p2_score, p1_pos, p2_pos, is_p1):
	values = {3: 1, 4: 3, 5: 6, 6: 7, 7: 6, 8: 3, 9: 1}
	s = [0, 0]
	for k, v in values.items():
		new = run_with_rolls(k, p1_score, p2_score, p1_pos, p2_pos, is_p1)
		s[0] += v * new[0]
		s[1] += v * new[1]
	return s

def run_with_rolls(roll_sum, p1_score, p2_score, p1_pos, p2_pos, is_p1):
	if is_p1:
		p1_pos = (((p1_pos - 1) + roll_sum) % 10) + 1
		p1_score += p1_pos
		if p1_score >= 21:
			return [1, 0]
		else:
			return run_many_worlds(p1_score, p2_score, p1_pos, p2_pos, False)
	else:
		p2_pos = (((p2_pos - 1) + roll_sum) % 10) + 1
		p2_score += p2_pos
		if p2_score >= 21:
			return [0, 1]
		else:
			return run_many_worlds(p1_score, p2_score, p1_pos, p2_pos, True)

def part2(i):
	counts = run_many_worlds(0, 0, i[0], i[1], True)
	return max(counts[0], counts[1])

def main():
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	print(part2(i))

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
