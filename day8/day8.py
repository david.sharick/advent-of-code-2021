#!/usr/bin/env python3

import sys
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce"""
		self.testlines = parse_input(self.testinput)
		self.basicinput = "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf"
		self.basiclines = parse_input(self.basicinput)
	
	def test_is_digit_1478(self):
		self.assertTrue(is_digit_1478("edbc"))
		self.assertTrue(is_digit_1478("eag"))
		self.assertTrue(is_digit_1478("fb"))
		self.assertTrue(is_digit_1478("edbcagd"))
	
	def test_part1(self):
		self.assertEqual(part1(self.testlines), 26)
	
	def test_remove_chars_from_string(self):
		self.assertEqual(remove_chars_from_string("abcde", "abdc"), "e")
	
	def test_str_intersect(self):
		self.assertEqual(str_intersect(["abc", "cde"]), "c")
		self.assertEqual(str_intersect(["abc", "def"]), "")
		self.assertEqual(str_intersect(["abcdgh", "cfhde", "ches"]), "ch")
	
	def test_decode_line(self):
		self.assertEqual(decode_line(self.basiclines[0]), {"a": "d", "b": "e", "c": "a", "d": "f", "e": "g", "f": "b", "g": "c"})
	
	def test_get_value(self):
		self.assertEqual(get_value(self.basiclines[0]), 5353)
	
	def test_part2(self):
		self.assertEqual(part2(self.basiclines), 5353)
		self.assertEqual(part2(self.testlines), 61229)


def parse_input(i):
	"""Parses an input given as a string"""
	lines = []
	for line in i.split("\n"):
		#print(line)
		l = line.split(" | ")
		#print(l)
		lines.append([l[0].split(), l[1].split()])
	return lines

def part1(i):
	"""Counts the number of digits 1, 4, 7, and 8 in the digits of an output"""
	out = 0
	for line in i:
		out += len([digit for digit in line[1] if is_digit_1478(digit)])
	return out

def part2(i):
	"""Sums the values of all lines in the input"""
	return sum(get_value(l) for l in i)

def remove_chars_from_string(src, chars):
	"""Removes all characters in chars from the source string"""
	out = src
	for char in chars:
		out = out.replace(char, "")
	return out

def str_intersect(strings):
	"""Calculates the per-character intersection of all strings in a list"""
	return "".join(char for char in strings[0] if all([char in s for s in strings[1:]]))

def decode_line(l):
	"""Converts a single line to a set of digits, and returns the mapping it uses"""
	ALL = "abcdefg"
	mapping = {}
	sixes = []
	fives = []
	for value in l[0]:
		if len(value) == 2:
			one = value
		elif len(value) == 3:
			seven = value
		elif len(value) == 4:
			four = value
		elif len(value) == 5:
			fives.append(value)
		elif len(value) == 6:
			sixes.append(value)
	
	mapping["c"] = one
	mapping["f"] = one
	
	# we can figure the exact a
	mapping["a"] = remove_chars_from_string(seven, mapping["c"])
	
	# bd equals the rest
	rest = remove_chars_from_string(four, mapping["c"])
	mapping["b"] = rest
	mapping["d"] = rest
	
	# now we know a, half-know c, f, b, d, don't know e, g
	# to get d|g, intersect all 5s and remove known a
	# distinguish them with: d in all 6s, g in 2/3
	
	d_or_g = str_intersect(fives).replace(mapping["a"], "")
	fst = d_or_g[0]
	snd = d_or_g[1]
	if all([fst in six for six in sixes]):
		mapping["g"] = fst
		mapping["d"] = snd
	else:
		mapping["g"] = snd
		mapping["d"] = fst
	
	# now we know adg, don't know cfeb
	# can use sixes to split cf from eb, or fives as well
	# but first use d to get b
	mapping["b"] = mapping["b"].replace(mapping["d"], "")
	
	# now we know abdg
	# need cef
	# start with f: it's in all 3 6s
	unknown = remove_chars_from_string(ALL, mapping["a"] + mapping["b"] + mapping["d"] + mapping["g"])
	f = str_intersect(sixes + [unknown])
	mapping["f"] = f
	
	# now we only need c or e, and then can get other
	# because we have f we can get c
	c = remove_chars_from_string(mapping["c"], mapping["f"])
	mapping["c"] = c
	
	# e is remaining
	mapping["e"] = remove_chars_from_string(ALL, mapping["a"] + mapping["b"] + mapping["c"] + mapping["d"] + mapping["f"] + mapping["g"])
	
	return mapping

def get_value(line):
	"""Gets the total value of a line"""
	mapping = decode_line(line)
	reversed_mapping = {v: k for k, v in mapping.items()}
	new_digits = ["".join(sorted(digit.translate(str.maketrans(reversed_mapping)))) for digit in line[1]]
	digit_dict = {"abcefg": "0", "cf": "1", "acdeg": "2", "acdfg": "3", "bcdf": "4", "abdfg": "5", "abdefg": "6", "acf": "7", "abcdefg": "8", "abcdfg": "9"}
	return int("".join(digit_dict[d] for d in new_digits))

def is_digit_1478(d):
	"""Returns whether a digit is one of 1, 4, 7, or 8 by checking its segment count length"""
	l = len(d)
	return (l == 2) or (l == 3) or (l == 4) or (l == 7)

def main():
	if len(sys.argv) < 2:
		sys.exit(0)
	lines = []
	with open(sys.argv[1], "r") as f:
		for line in f.readlines():
			if line == "\n":
				break
			l = line.split(" | ")
			lines.append([l[0].split(), l[1].split()])
	print(part1(lines))
	print(part2(lines))

if __name__ == '__main__':
	#unittest.main()
	main()
